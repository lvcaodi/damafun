/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50128
Source Host           : localhost:3306
Source Database       : db_oldama1

Target Server Type    : MYSQL
Target Server Version : 50128
File Encoding         : 65001

Date: 2015-11-02 18:35:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cz_admin`
-- ----------------------------
DROP TABLE IF EXISTS `cz_admin`;
CREATE TABLE `cz_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `allow` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `cz_cat`
-- ----------------------------
DROP TABLE IF EXISTS `cz_cat`;
CREATE TABLE `cz_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `cz_comment`
-- ----------------------------
DROP TABLE IF EXISTS `cz_comment`;
CREATE TABLE `cz_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `comment` text,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`),
  KEY `uid` (`uid`),
  CONSTRAINT `cz_comment_ibfk_1` FOREIGN KEY (`vid`) REFERENCES `cz_video` (`id`),
  CONSTRAINT `cz_comment_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `cz_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `cz_user`
-- ----------------------------
DROP TABLE IF EXISTS `cz_user`;
CREATE TABLE `cz_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `allow` tinyint(1) NOT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '1',
  `tel` varchar(20) DEFAULT '',
  `email` varchar(20) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `cz_video`
-- ----------------------------
DROP TABLE IF EXISTS `cz_video`;
CREATE TABLE `cz_video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desn` text,
  `ptime` int(11) NOT NULL DEFAULT '0',
  `pic` varchar(50) NOT NULL DEFAULT '',
  `hot` int(11) NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `comnumber` int(11) NOT NULL DEFAULT '0',
  `path` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `uid` (`uid`),
  CONSTRAINT `cz_video_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `cz_cat` (`id`),
  CONSTRAINT `cz_video_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `cz_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

